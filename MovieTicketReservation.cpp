/*
// Sample code to perform I/O:

cin >> name;                            // Reading input from STDIN
cout << "Hi, " << name << ".\n";        // Writing output to STDOUT

// Warning: Printing unwanted or ill-formatted data to output will cause the test cases to fail
*/

// Write your code here
#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <unordered_map>
#include <cstring>

using namespace std;

class screen{
    string screenName;
    int numberOfRows;
    int seatsPerRow;
    vector<int> aisleSeats;
    vector<vector<bool> > seats;    //false-> seat is empty, true->seat is already reserved

    public:
    screen(string screenName, int numberOfRows, int seatsPerRow, vector<int> aisleSeats){
        this->screenName = screenName;
        this->numberOfRows = numberOfRows;
        this->seatsPerRow = seatsPerRow;
        this->aisleSeats = aisleSeats;
        sort(aisleSeats.begin(), aisleSeats.end());
        vector<bool> v(seatsPerRow, false);
        vector<vector<bool> > seats(numberOfRows, v);
        this->seats = seats;
    }
    bool reserveSeats(int rowNumber, vector<int> listOfSeats){
        for(int i=0; i<listOfSeats.size(); i++){
            int currSeat = listOfSeats[i];
            if(currSeat<0){
                return false;
            }
            if(seats[rowNumber][currSeat]){
                return false;
            }
        }
        for(int i=0; i<listOfSeats.size(); i++){
            int currSeat = listOfSeats[i];
            seats[rowNumber][currSeat] = true;
        }
        return true;
    }
    vector<int> getUnreservedSeats(int rowNumber){
        vector<int> unreservedSeats;
        for(int i=0; i<seats[rowNumber].size(); i++){
            if(!seats[rowNumber][i]){
                unreservedSeats.push_back(i);
            }
        }
        return unreservedSeats;
    }
    vector<int> suggestSeats(int numberOfSeats, int rowNumber, int seatNumber){
        int i=0;   //corresponding to aisle seat
        for(i=0; i<aisleSeats.size(); i++){
            if(aisleSeats[i]>seatNumber){
                break;
            }
        }
        if(aisleSeats[i]<seatNumber){
            int lowerLimit = aisleSeats[i];
            vector<int> v;
        }
    }
};

class theatre{
    //map of all screens: key-screen name
    int numberOfScreens;
    unordered_map<string, screen> screensList;
    
    public:
    theatre(){
        this->numberOfScreens = 0;
    } 
    bool addScreen(string screenName, int numberOfRows, int seatsPerRow, vector<int> aisleSeats){
        if(screensList.find(screenName)!=screensList.end()){
            return false;
        }
        if(numberOfRows<=0 || seatsPerRow<=0){
            return false;
        }
        for(int i=0; i<aisleSeats.size(); i++){
            if(aisleSeats[i]<0){
                return false;
            }
        }
        screen newScreen(screenName, numberOfRows, seatsPerRow, aisleSeats);
        screensList[screenName] = newScreen;
        numberOfScreens++;
        return true;
    }
    bool reserveSeats(string screenName, int rowNumber, vector<int> listOfSeats){
        if(rowNumber<0){
            return false;
        }
        if(screensList.find(screenName)==screensList.end()){
            return false;
        }
        screen currentScreen = screensList[screenName];
        if(!currentScreen.reserveSeats(rowNumber, listOfSeats)){
            return false;
        }
        screensList[screenName] = currentScreen;
        return true;
    }
    vector<int> getUnreservedSeats(string screenName, int rowNumber){
        if(rowNumber<0){
            vector<int> v;
            return v;
        }
        if(screensList.find(screenName) == screensList.end()){
            vector<int> v;
            return v;
        }
        screen currentScreen = screensList[screenName];
        vector<int> unreservedSeats = currentScreen.getUnreservedSeats(rowNumber);
        return unreservedSeats;
    }
    vector<int> suggestSeats(string screenName, int numberOfSeats, int rowNumber, int seatNumber){
        if(rowNumber<0 || seatNumber<0 || numberOfSeats<=0){
            vector<int> v;
            return v;
        }
        if(screensList.find(screenName) == screensList.end()){
            vector<int> v;
            return v;
        }

    }

};

int main(){

    return 0;
}